package com.walterfinkbeiner.apitests.miniframework;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * @author Walter Finkbeiner
 */
class RetrofitSingleton {

    private static Retrofit instance;

    private RetrofitSingleton() {
    }

    static Retrofit getInstance() {
        String baseUrl = System.getProperty("baseUrl") != null ? System.getProperty("baseUrl") : "http://127.0.0.1:8080";
        if (instance == null) {
            instance = new Retrofit.Builder().baseUrl(baseUrl)
                    .addConverterFactory(JacksonConverterFactory.create())
                    .build();
        }
        return instance;
    }

}
