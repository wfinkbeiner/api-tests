package com.walterfinkbeiner.apitests.miniframework;

import java.lang.reflect.ParameterizedType;

/**
 * @author Walter Finkbeiner
 */
public abstract class ApiTest<T> {

    private final T api;

    protected ApiTest() {
        Class<T> serviceToTest = getApiType();
        this.api = RetrofitSingleton.getInstance().create(serviceToTest);
    }

    @SuppressWarnings("unchecked")
    private Class<T> getApiType() {
        ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
        return (Class<T>) parameterizedType.getActualTypeArguments()[0];
    }

    protected T getApi() {
        return api;
    }
}

