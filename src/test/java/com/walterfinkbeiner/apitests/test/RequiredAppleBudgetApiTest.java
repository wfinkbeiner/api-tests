package com.walterfinkbeiner.apitests.test;

import com.walterfinkbeiner.apitests.api.RequiredAppleBudgetApi;
import com.walterfinkbeiner.apitests.miniframework.ApiTest;
import com.walterfinkbeiner.apitests.model.CustomError;
import com.walterfinkbeiner.apitests.model.RequiredAppleBudget;
import org.testng.ITest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.lang.reflect.Method;

import static com.walterfinkbeiner.apitests.miniframework.TestUtils.*;
import static org.testng.Assert.assertEquals;

/**
 * @author Walter Finkbeiner
 */
public class RequiredAppleBudgetApiTest extends ApiTest<RequiredAppleBudgetApi> implements ITest {

    private ThreadLocal<String> testName = new ThreadLocal<>();

    @Override
    public String getTestName() {
        return testName.get();
    }

    @BeforeMethod
    public void BeforeMethod(Method method, Object[] testData) {
        testName.set(String.format("%s %s", method.getDeclaredAnnotation(Test.class).testName(), testData[4]));
    }

    @DataProvider(name = "Valid Input Data")
    public static Object[][] validInput() {
        return new Object[][]{
                {"20180801", "1", 200, 0.05, "with minimum days amount"},
                {"20180101", "365", 200, 21.32, "with maximum days amount"},
                {"20200228", "2", 200, 0.08, "in a leap year"},
                {"20200229", "1", 200, 0.04, "with a leap day as the date parameter"},
                {"20180820", "20", 200, 0.98, "between two months"},
                {"20180801", "10", 200, 0.56, "within the same month"},
                {"20180801", "7", 200, 0.35, "with days from 1 to 7"},
                {"20180808", "7", 200, 0.49, "with days from 8 to 14"},
                {"20180815", "7", 200, 0.56, "with days from 15 to 21"},
                {"20180822", "10", 200, 0.40, "with days from 22 to 31"}
        };
    }

    @DataProvider(name = "Invalid Input Data")
    public static Object[][] invalidInput() {
        return new Object[][]{
                {null, "1", 400, "Required parameter 'date' is not present", "without date parameter"},
                {"20180810", null, 400, "Required parameter 'days' is not present", "without days parameter"},
                {"20180140", "1", 400, "'20180140' is not a valid value for 'date'", "with invalid date that has the correct format"},
                {"20180229", "1", 400, "'20180229' is not a valid value for 'date'", "with a leap day on a non-leap year"},
                {"2018012", "1", 400, "'2018012' is not a valid value for 'date'", "with incorrect format in date parameter"},
                {"20180810", "string", 400, "'string' is not a valid value for 'days'", "with incorrect format in days parameter"},
                {"20180810", "-1", 400, "'days' must be greater than 0", "with negative days parameter"},
                {"20180810", "0", 400, "'days' must be greater than 0", "with days  = 0"},
                {"20180810", "366", 400, "'days' must be equal or lower than 365", "with days parameter higher than the maximum"}
        };
    }

    @Test(dataProvider = "Valid Input Data", testName = "Valid request")
    public void positiveTest(String date, String days, int expectedCode, Double expectedBudget, String specificCase) throws IOException {
        Call<RequiredAppleBudget> call = getApi().getRequiredAppleBudget(date, days);
        Response<RequiredAppleBudget> response = call.execute();
        assertEquals(response.code(), expectedCode,
                String.format("The status code of the response when making a %s request to %s (%s) should be %s.",
                        call.request().method(), call.request().url(), specificCase, expectedCode));
        assertEquals(
                response.body().getBudget(), expectedBudget, 0,
                String.format("The budget returned when making a %s request to %s (%s) should be %s.",
                        call.request().method(), call.request().url(), specificCase, expectedBudget));
    }

    @Test(dataProvider = "Invalid Input Data", testName = "Invalid request")
    public void negativeTest(String date, String days, int expectedCode, String expectedErrorMessage, String specificCase) throws IOException {
        Call<RequiredAppleBudget> call = getApi().getRequiredAppleBudget(date, days);
        Response<RequiredAppleBudget> response = call.execute();
        CustomError customError = convertResponse(CustomError.class, response.errorBody());
        assertEquals(response.code(), expectedCode,
                String.format("The status code of the response when making a %s request to %s (%s) should be %s.",
                        call.request().method(), call.request().url(), specificCase, expectedCode));
        assertEquals(customError.getError(), expectedErrorMessage,
                String.format("The error body returned when making a %s request to %s (%s) should be ''%s''.",
                        call.request().method(), call.request().url(), specificCase, expectedErrorMessage));
    }
}
