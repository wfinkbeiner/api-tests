package com.walterfinkbeiner.apitests.model;

/**
 * @author Walter Finkbeiner
 */
public class RequiredAppleBudget {

    private Double budget;

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }
}

